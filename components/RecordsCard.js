import {useState, useEffect} from 'react'

import Card from 'react-bootstrap/Card'
import {Fragment} from 'react'

export default function Record({prop}){

	const {_id, recordName, recordType, category, amount, dateMade} = prop

	return(
		<Fragment>
			<Card>
				<h5>{recordName}</h5>
				<h6><span className="text-danger">{recordType}</span> ({category})</h6>
				<p>{amount}</p>
				<p>{dateMade}</p>
			</Card>
		</Fragment>


		)

}