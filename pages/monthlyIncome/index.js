import {useEffect} from 'react'
import {VerticalBar} from 'react-chartjs-2'

export default function monthlyIncome(){
	return(
		<>
			<h2 className="text-center">Monthly Income</h2>
			<canvas id="canvas" className="responsive" style={{border: '1px solid black', display: 'block'}} />
		</>
		)
}