import {useState,useEffect} from 'react'
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container} from 'react-bootstrap'
import NavBar from '../components/NavBar'
import {UserProvider} from '../UserContext'

function MyApp({ Component, pageProps }) {


  const [user,setUser] = useState({

    id: null,
    email: null   

  })


useEffect(()=>{

      fetch('http://localhost:4000/api/users/details',{

        headers: {

          Authorization: `Bearer ${localStorage.getItem('token')}`

        }

      })
      .then(res => res.json())
      .then(data => {

          if(data._id){

              setUser({

              id: data._id,
              email: data.email              

          })


          } else {

              setUser({

              id: null,
              email: null
          })


        }

        

      })


  }, []) 


  const unsetUser = () => {
    localStorage.clear()
    setUser({
      id: null,
      email: null,
    })
  }



  return (

    <UserProvider value={{user,setUser,unsetUser}}>
    
   <NavBar />
   <Container>
      <Component {...pageProps} />
   </Container>

  </UserProvider>

    )
}

export default MyApp
