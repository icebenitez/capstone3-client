import {useState,useEffect,useContext} from 'react'

import Record from '../../components/RecordsCard'
import AppHelper from '../../app_helper.js'

import {Fragment} from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

export default function Records() {

  const [records, setRecords] = useState([])

  useEffect(() => {
    const option = {
      headers: {
        Authorization: `Bearer ${AppHelper.getAccessToken()}`
      }
    }

    fetch(`${AppHelper.API_URL}/users/records`, option)
    .then(AppHelper.toJSON)
    .then(data => {
      //console.log(data)
      const allRecords = data.map(record => {
        return <Record key={record._id} prop={record}/>
      })
      setRecords(allRecords)
    })
    
  },[records])

  const [budget, setBudget] = useState(0)

  useEffect(()=>{
    console.log({records})
  }, [])

  return (

    <>
  		<div className="pt-3 mb-5 container">
  		<h3>Records</h3>
  		<Form> 		
  			
  			<Form.Group>
  			<div className="mb-2 input-group">
  				<Button href="/records/new" className="btn btn-success" >Add</Button>
  				<Form.Control type="text" placeholder="Search Record" className="form-control" />
  				<select className="form-control">
		  			<option value="All" selected>All</option>
		  			<option value="Income">Income</option>
		  			<option value="Expenses">Expense</option>
		  		</select>
				</div>
  			</Form.Group>
  			
  		</Form>
      {records}
  		</div>
    </>
  	)
}

