import {useState,useEffect,useContext} from 'react'

import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Card from 'react-bootstrap/Card'
import {Row,Col} from 'react-bootstrap'

import AppHelper from '../../app_helper.js'

export default function newRecord() {

  const [allCategories, setAllCategories] = useState([])
  const [sortedCategories, setSortedCategories] = useState([])
  const [category, setCategory] = useState('')
  const [categoryType, setCategoryType] = useState('')
  const [amount, setAmount] = useState(0)
  const [recordName, setRecordName] = useState('')
  //const [records, setRecords] = useState([])

  //console.log(categories)

  //this will run on the first render to get the categories made by the user
  useEffect(() => {

    const tempCats = []

    const info = {
      headers: {
        Authorization: `Bearer ${AppHelper.getAccessToken()}`
      }
    }

    fetch(`${AppHelper.API_URL}/users/categories`, info)
    .then(AppHelper.toJSON)
    .then(data => {
      //console.log(data)
      data.map(element => tempCats.push(element))
      setAllCategories(tempCats)
      //console.log(allCategories)
    })

  }, [])
  //this will find all the categories under the type chosen from dropdown
  useEffect(() =>{
    //console.log(categoryType)
    console.log(allCategories)
    /*allCategories.forEach(element => {
      if(element.categoryType === categoryType){
        console.log(true)
      } else {
        console.log(false)
      }
    })*/
  },[categoryType])

  return (

  		<div className="mt-5 pt-4 mb-5 container">
  		  
      <Row className="justify-content-center">
        <Col md={6}>
          <h3>New Record</h3>
          <Card>
            <Card.Header>Record Information</Card.Header>
            <Card.Body>
              <Form>
                <Form.Group>
                  <Form.Label htmlFor="typeName">
                    Category Type:
                  </Form.Label>
                  <Form.Control 
                  as="select" 
                  required 
                  onChange={e => setCategoryType(e.target.value)} 
                  value={categoryType} 
                  id="typeName">
                    <option value="true" disabled="">Select Category</option>                    
                    <option value="Income">Income</option>
                    <option value="Expenses">Expense</option>
                  </Form.Control>
                </Form.Group>

                <Form.Group>
                  <Form.Label>
                    Category Name:
                  </Form.Label>
                  <Form.Control
                  as="select" 
                  required>
                    <option value="true" disabled="">Select Category</option>                                        
                  </Form.Control>
                </Form.Group>

                <Form.Group>
                  <Form.Label>
                    Amount:
                  </Form.Label>
                  <Form.Control placeholder="Enter Amount" required type="number" value={amount} />
                </Form.Group>
                <Form.Group>
                  <Form.Label>
                    Description:
                  </Form.Label>
                  <Form.Control type="text" placeholder="Enter Description" value={recordName} required />
                </Form.Group>
              </Form>
              <Button type="submit" className="btn btn-primary">Submit</Button>
            </Card.Body>
          </Card>

        </Col>        
      </Row>

      </div>

  	)
}